import React, { useEffect, useState } from 'react'
import Axios from "axios";
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import './ReviewPageComponents.css'
import { fetchMovieAll } from '../../../state/showListAll/showListAllActions';

const ReviewPageComponents = () => {

  const {filmID} = useParams()
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);

  useEffect(() => {
    fetch(`https://team-b.gabatch11.my.id/user/${profileID}`)
      .then(res => res.json())
      .then(data => {
        setDataProf(data.data)
      })
  })

  const [dataReview, setDataReview] = useState([])

  useEffect(() => {
    fetch(`https://team-b.gabatch11.my.id/review/?movieId=${filmID}&page=1`)
      .then(res => res.json())
      .then(data => {
        setDataReview(data.data.docs)
      })
  })

  const film = useSelector (state =>
    state.showMovieAllReducer.moviesall.find(filma => filma._id === filmID)
  )

  const [title, setTitle]= useState("");
  const [content, setContent]= useState("");
  const [rating, setRating]= useState(4);

  async function submitReview() {
    let item = {title, content, rating}
    console.log(item)
    const localToken = localStorage.getItem("token")
  
    console.log(filmID)

    const config = {
      headers: { 'Authorization': `Bearer ${localToken}`}
    }

    console.log(config)
    const post = await Axios.post (
      
     `https://team-b.gabatch11.my.id/review/?movieId=${filmID}`,
      {
        title,
        content,
        rating,
      },
      config
    ) .then(console.log).catch(console.log);
    
    // console.log(post.data.message)
    // console.log(post.data.token);
    // console.log(post.data.id);
    // localStorage.setItem("id", post.data.id);
    // localStorage.setItem("token", post.data.token);;
    // history.push('/profile');
  }

  const profileID = localStorage.getItem("id")
  const [dataProf, setDataProf] = useState([])
  
  const reviewBox = () => {
    if (!dataProf) {
      return (
        <div></div>
      )
    } return (
        <div className="review-form">
          <div className="form-identity">
            <div className="identity-photo">
              <img src = "https://i.ibb.co/gFPq1P4/blank-profile-picture-973460-1280.png" alt="" />
            </div>
            <div className="identity-data">
              <h2>
                {dataProf.fullname}
              </h2>
            </div>
          </div>
          <div className="form-input">
            <input className="title-input" type="text" name="title" placeholder="Judul ulasan kamu disini" value={title} onChange={(e) => setTitle(e.target.value)} />
            <input className="content-input" type="text" name="content" placeholder="Ulasan kamu disini" value={content} onChange={(e) => setContent(e.target.value)} />
          </div>
          <div className="button-submit" > 
            <button onClick={submitReview}>Submit</button>
          </div>
        </div>
    )
  }

  const buttonDetail = () => {
    if (!film) {
      return (
        <div>nothing</div>
      )
    } return (
      <div className="button-detail-container">
        <div className="button-detail-wrapping">
          <div className="button-detail-category-bar">
            <Link to={'/'}>
              <button>Home</button>
            </Link>
            <Link to={`/view/${film._id}`}>
              <button>Overview</button>
            </Link>
            <Link to={`/cast/${film._id}`}>
              <button>Character</button>
            </Link>
            <Link to={`/review/${film._id}`}>
              <button>Review</button>
            </Link>
          </div>
        </div>
      </div>
    )
  }

  const tampilinReview = () => {
    if (!dataReview) {
      return (
        <div>Belum ada Review</div>
      )
    } return dataReview.map (item => 
        <div className="review-context">
          <div className="review-identity">
            <div className="review-photo">
              <img src="https://i.ibb.co/gFPq1P4/blank-profile-picture-973460-1280.png" alt="" />
            </div>
            <div className="review-data">
              <p className="title-review">{item.title}</p>
              <p className="content-review">{item.content}</p>
            </div>
          </div>
        </div>
    )
  }

  return (
    <div>
      {buttonDetail()}
      <div className="review-container">
        <div className="review-wrapping">
          {reviewBox()}
          {tampilinReview()}
        </div>
      </div>
    </div>
  )
}

export default ReviewPageComponents
