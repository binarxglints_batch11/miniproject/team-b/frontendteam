import React from 'react'
import HeaderHero from '../../common/header-hero'
import ReviewPageComponents from './components/ReviewPageComponents'

const ReviewPageContainers = () => {
  return (
    <div>
      <HeaderHero/>
      <ReviewPageComponents/>
    </div>
  )
}

export default ReviewPageContainers
