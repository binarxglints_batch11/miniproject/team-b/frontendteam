import React from 'react';
import ButtonCategory from '../../common/buttonCategory';
import HeaderHeroMain from '../../common/header-hero-main';
import FilterPageComponents from './components/FilterPageComponents'

function FilterPageContainers () {
  return (
    <div>
      <HeaderHeroMain/>
      <ButtonCategory/>
      <FilterPageComponents/>
    </div>
  )
}

export default FilterPageContainers;