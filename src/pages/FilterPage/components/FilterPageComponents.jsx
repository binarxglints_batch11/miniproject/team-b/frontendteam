import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchMovieAll } from '../../../state/showListAll/showListAllActions';
import { Link } from "react-router-dom";
import './FilterPageComponents.css';

function FilterPageComponents (){
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);  

  const states = useSelector((state) => state);

  const cobapost = () => {
    if (states.showMovieAllReducer.loading) {
      var iniLoading = [...Array(10).keys()] 
      return iniLoading.map ((el) => {
        return (
          <a href = "#"  >
            <div className="film-item">
              <div className="film-cover hp-loading">
                <img src="" alt=""/>
              </div>
            </div>
          </a>
        )
      })
    }
      return states.showMovieAllReducer.moviesall.filter(o => Object.keys(o).some(k => String(o[k]).toLowerCase().includes(states.searchReducer.search.toLowerCase()))).map ((el)=>{
        return (
            <div className="film-item">
              <div className="film-cover">
                <div className="overlay-category">
                  <Link to={`/view/${el._id}`}>{el.title}</Link>
                </div>
                <img src={`https://team-b.gabatch11.my.id${el.poster}`} alt={el.title}/>
              </div>
              <h3>{el.title}</h3>
            </div>
        )
    })

  }

  return (
    <div className="filter-container">
      <div className="filter-wrapping">
        <div className="film-detail">
          {cobapost()}
        </div>
        <div className="pagination-numb">
        </div>
      </div>
    </div>
  )
}

export default FilterPageComponents;