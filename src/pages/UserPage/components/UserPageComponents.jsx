import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import './UserPageComponents.css'
import { useParams } from 'react-router';
import { useHistory } from "react-router-dom";

const UserPageComponents = () => {

  const { profID } = useParams()

  const history = useHistory()
  const states = useSelector((state) => state)
  const [dataProf, setDataProf] = useState([])

  const profileID = localStorage.getItem("id")

  useEffect(() => {
    fetch(`https://team-b.gabatch11.my.id/user/${profileID}`)
      .then(res => res.json())
      .then(data => {
        setDataProf(data.data)
      })
  })

  const clearButton = () => {
    localStorage.clear();
    history.push('/')
  }

  const postUser = () => {
    if (!dataProf) {
      return (
        <div>a</div>
      )
    } return (
      <div className="profile-wrapping">
        <div className="profile-image">
          <img src="https://i.ibb.co/gFPq1P4/blank-profile-picture-973460-1280.png" alt="Profile" border={0} />
        </div>
        <div className="profile-data">
          <div className="profile-name">
            <h1>{dataProf.fullname}</h1>
          </div>
          <div className="profile-contact">
            <p>{`${dataProf.username}   ||   ${dataProf.email}`}</p>
          </div>
          <div className="button-edit">
            <button>
              Edit
            </button>
            <button onClick={clearButton}>
              Logout
            </button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="profile-container">
      {postUser()}
    </div>
  )
}

export default UserPageComponents
