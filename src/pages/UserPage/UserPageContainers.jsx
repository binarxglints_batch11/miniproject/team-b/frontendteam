import React from 'react'
import UserPageComponents from './components/UserPageComponents'

const UserPageContainers = () => {
  return (
    <div>
      <UserPageComponents/>
    </div>
  )
}

export default UserPageContainers
