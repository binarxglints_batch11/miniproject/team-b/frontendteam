import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

const Register = (props) => {
  const [fullname, setfullName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmpassword, setconfirmPassword] = useState("");

  const history = useHistory()

  async function Submit() {
    let item = {fullname, username, email, password, confirmpassword}
    console.log(item)
    const post = await axios.post (
      "https://team-b.gabatch11.my.id/signup",
      {
        fullname,
        username,
        email,
        password,
        confirmpassword,
      }
    )
    
    // console.log(post.data.message);
    // console.log(post.data.token);
    // console.log(post.data.id)
    localStorage.setItem("id", post.data.id);
    localStorage.setItem("token", post.data.token);
    history.push('/profile');
  }
  return (
    <div className="base-container" ref={props.containerRef}>
      <div className="header">Sign Up</div>
      <div className="content">
        <div className="image">
          <img src="https://i.ibb.co/8D2q2sr/Dark-Logo.png" alt="Dark Logo Wibu TV"/>
        </div>
        <div className="form">
        <div className="form-group">
            <label htmlFor="username">Fullname</label>
            <input type="text" name="username" placeholder="username" value={fullname} onChange={(e) => setfullName(e.target.value)}/>
          </div>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input type="text" name="username" placeholder="username" value={username} onChange={(e) => setUsername(e.target.value)}/>
          </div>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input type="email" name="email" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input type="password" name="password" placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
          </div>
          <div className="form-group">
            <label htmlFor="password">Confirm Password</label>
            <input type="password" name="password" placeholder="password" value={confirmpassword} onChange={(e) => setconfirmPassword(e.target.value)}/>
          </div>
        </div>
      </div>
      <div className="footerloginsignup">
        <button type="button" className="btn" onClick={Submit}>
          Sign Up
        </button>
      </div>
    </div>
  );
}

export default Register;
    