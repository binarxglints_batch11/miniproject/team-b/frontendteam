import Axios from "axios";
import React, {useState} from "react";
import { useHistory } from "react-router-dom";

const Login = (props) => {
  const [email, setEmail]= useState("");
  const [password, setPassword]= useState("");
  const [error, setError] = useState("");

  const history = useHistory()

  async function Submit() {
    let item = {email, password}
    console.log(item)
    const post = await Axios.post (
      
     "https://team-b.gabatch11.my.id/login",
      {
        email,
        password,
      }
    )
    // console.log(post.data.message)
    // console.log(post.data.token);
    // console.log(post.data.id);
    localStorage.setItem("id", post.data.id);
    localStorage.setItem("token", post.data.token);;
    history.push('/profile');
  }

  return (
      <div className="base-container" ref={props.containerRef}>
        <div className="header">Login</div>
        <div className="content">
          <div className="image">
          <img src="https://i.ibb.co/8D2q2sr/Dark-Logo.png" alt="Dark Logo Wibu TV"/>
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input type="email" name="email" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type="password" name="password" placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
          </div>
        </div> <br/>
        {error && <div className='error'>{error}</div>}
        <div className="footer-signup">
          <button type="button" className="btn" onClick={Submit}>
            Login
          </button>
        </div>
      </div>
  )
}

export default Login;