import React, {useEffect}from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { fetchMovieAll } from '../../../state/showListAll/showListAllActions';
import './ViewPage.css';

const ViewPage = () => {

  const {filmID} = useParams()

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);

  const film = useSelector (state =>
    state.showMovieAllReducer.moviesall.find(filma => filma._id === filmID)
  )

  const buttonDetail = () => {
    if (!film) {
      return (
        <div></div>
      )
    } return (
      <div className="button-detail-container">
        <div className="button-detail-wrapping">
          <div className="button-detail-category-bar">
            <Link to={'/'}>
              <button>Home</button>
            </Link>
            <Link to={`/view/${film._id}`}>
              <button>Overview</button>
            </Link>
            <Link to={`/cast/${film._id}`}>
              <button>Character</button>
            </Link>
            <Link to={`/review/${film._id}`}>
              <button>Review</button>
            </Link>
          </div>
        </div>
      </div>
    )
  }

  const synopsis = () => {
    if (!film) {
      return (
        <div className="not-detail">
          <div className = "not-found-container">
            <div className = "not-found-wrapper">
              <div className= "not-found-detail">
                <div className= "head-not"> Oops!</div>
                <h2> 404 PAGE NOT FOUND </h2>
                <p> The page you are looking for might have been removed <span>
                  had its name changed or is temporarty unavailable </span></p>
              </div>
            </div>
          </div>
        </div>
      )
    } return (
        <>
        <h1>
          <span>Synopsis</span>
        </h1>
        <p>
          <span>{film.synopsis}</span>
        </p>
        </>
    )
  }

  return (
    <div>
      {buttonDetail()}
      <div className="container-viewpage">
        {synopsis()}
        {/* <h1>
          <span>Movie Info</span>
        </h1>
        <p>
          <strong>Release date :</strong>
          January 5, 1998
        </p>
        <p>
          <strong>Director : </strong>
          John Doe
        </p>
        <p>
          <strong>Featured song :</strong>
          Pegasus fantasi
        </p>
        <p>
          <strong>Budget :</strong>
          200 million USD
        </p> */}
      </div>
    </div>
  );
};
export default ViewPage;
