import React from 'react'
import HeaderHero from '../../common/header-hero'
import ViewPage from './components/ViewPage'

const ViewPageContainers = () => {
  return (
    <div>
      <HeaderHero/>
      <ViewPage/>
    </div>
  )
}

export default ViewPageContainers
