import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import './CharacterPageComponents.css'
import {fetchCast} from '../../../state/showCast/showCastActions'
import { fetchMovieAll } from '../../../state/showListAll/showListAllActions';

const CharacterPageComponents = () => {

  const dispatch = useDispatch()
  const {filmID} = useParams()

  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);

  const film = useSelector (state =>
    state.showMovieAllReducer.moviesall.find(filma => filma._id === filmID)  
  )

    const buttonDetail = () => {
    if (!film) {
      return (
        <div>nothing</div>
      )
    } return (
      <div className="button-detail-container">
        <div className="button-detail-wrapping">
          <div className="button-detail-category-bar">
            <Link to={'/'}>
              <button>Home</button>
            </Link>
            <Link to={`/view/${film._id}`}>
              <button>Overview</button>
            </Link>
            <Link to={`/cast/${film._id}`}>
              <button>Character</button>
            </Link>
            <Link to={`/review/${film._id}`}>
              <button>Review</button>
            </Link>
          </div>
        </div>
      </div>
    )
  }


  useEffect (() => {
    dispatch(fetchCast())
  })

  // const casting = useSelector (state =>
  //     state.showCastReducer.cast.filter(o => Object.keys(o).some(k => String(o[k]).toLowerCase().includes(filmID)))
  // )

  //   const castingPost = () => {
  //   if (casting == []) {
  //     <di>Kosong</di>
  //   }
  //     return casting.map ((el)=>{
  //       return (
  //         <div className="cast-item">
  //           <div className="cast-cover">
  //             <img src alt="" />
  //           </div>
  //           <h3>Saint Saiya</h3>
  //         </div>
  //       )
  //   })
  // }




  return (
    <div>
      {buttonDetail()}
      <div className="cast-container">
        <div className="cast-wrapping">
          <div className="cast-detail">
            {/* {castingPost()} */}
            <div className = "not-found-container">
              <div className = "not-found-wrapper">
                <div className= "not-found-detail">
                  <h1> Oops!</h1>
                  <h2> 404 PAGE NOT FOUND </h2>
                  <p> The page you are looking for might have been removed <span>
                    had its name changed or is temporarty unavailable </span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CharacterPageComponents  