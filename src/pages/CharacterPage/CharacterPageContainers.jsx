import React from 'react'
import HeaderHero from '../../common/header-hero'
import CharacterPageComponents from './components/CharacterPageComponents'

const CharacterPageContainers = () => {
  return (
    <div>
      <HeaderHero/>
      <CharacterPageComponents/>
    </div>
  )
}

export default CharacterPageContainers
