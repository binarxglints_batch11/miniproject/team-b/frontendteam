import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchMovie } from '../../../state/showList/showListActions';
import { Link } from "react-router-dom";
import './HomePageComponents2.css';

const HomePageComponents2 = () => {
  
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovie());
  },[]);

  useEffect(()=>{
    fetch('https://team-b.gabatch11.my.id/movie?limit=50')
      .then(res => res.json())
      .then(data=>{
          setPosts(data.data)
      })
      .then (setLoading(false))
  }) 

  const states = useSelector((state) => state);

  const [posts, setPosts]=useState([])
  const [loading, setLoading]=useState(true)
  const [pageNumber, setPageNumber]= useState(1)
  const [postNumber]= useState(10)


  const currentPageNumber = (pageNumber * postNumber) - postNumber  
  const paginatedPosts = posts.splice(currentPageNumber, postNumber)

  const maxPages = (Math.floor(states.showMovieReducer.movies.length / postNumber)) + 1

  const handlePrev =()=>{
      if(pageNumber === 1) {
        setPageNumber(pageNumber - 0)
      } else {
        setPageNumber(pageNumber - 1)
      }
  }

  const handleNext =()=>{
    if(pageNumber === maxPages) {
      setPageNumber(pageNumber + 0)
    } else {
      setPageNumber(pageNumber + 1)
    }
  }



  const buttonMainPost = () => {
    if (states.showMovieReducer.loading) {
      var iniLoading = [...Array(10).keys()] 
      return iniLoading.map ((el) => {
        return (
          <a href = "#"  >
            <div className="film-item">
              <div className="film-cover hp-loading">
                <img src="" alt=""/>
              </div>
            </div>
          </a>
        )
      })
    }
      return paginatedPosts.map(item =>
        <div className="film-item">
          <div className="film-cover">
            <div className="overlay-category">
              <Link to={`/view/${item._id}`}>{item.title}</Link>
            </div>
            <img src={`https://team-b.gabatch11.my.id${item.poster}`} alt={item.title}/>
          </div>
          <h3>{item.title}</h3>
        </div>
          )
  }

  return (
    <div className="homepage-container">
      <div className="homepage-wrapping">
        <div className="film-detail">
          {buttonMainPost()}
        </div>
      </div>
    <div className="button-container">
      <div className="button-wrapping">
        <button className="prev-button" onClick={handlePrev}>Prev</button>
        <button className="next-button" onClick={handleNext}>Next</button>
      </div>
    </div>
    </div>
  )
}

export default HomePageComponents2;