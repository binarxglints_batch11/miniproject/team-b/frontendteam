import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchMovie } from '../../../state/showList/showListActions';
import { Link } from "react-router-dom";
import './HomePageComponents.css';

function HomePageComponents (){
  const dispatch = useDispatch();
  const states = useSelector((state) => state);

  useEffect(() => {
    dispatch(fetchMovie());
  },[]);  

  const buttonMainPost = () => {
    if (states.showMovieReducer.loading) {
      var iniLoading = [...Array(10).keys()] 
      return iniLoading.map ((el) => {
        return (
          <a href = "#"  >
            <div className="film-item">
              <div className="film-cover hp-loading">
                <img src="" alt=""/>
              </div>
            </div>
          </a>
        )
      })
    }
      return states.showMovieReducer.movies.map ((el)=>{   
        const b64 = el.poster.toString('base64');
        return (
            <div className="film-item">
              <div className="film-cover">
                <div className="overlay-category">
                  <Link to={`/view/${el._id}`}>{el.title}</Link>
                </div>
                <img src={`https://team-b.gabatch11.my.id${el.poster}`} alt={el.title}/>
              </div>
              <h3>{el.title}</h3>
            </div>
        )
    })

  }

  return (
    <div className="homepage-container">
      <div className="homepage-wrapping">
        <div className="film-detail">
          {buttonMainPost()}
        </div>
        <div className="pagination-numb">
          {
            
          }
        </div>
      </div>
    </div>
  )
}

export default HomePageComponents;