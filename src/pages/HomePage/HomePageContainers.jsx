import React from 'react';
import ButtonCategory from '../../common/buttonCategory';
import HeaderHeroMain from '../../common/header-hero-main';
import HomePageComponents from './components/HomePageComponents'
import HomePageComponents2 from './components2/HomePageComponents2'

function HomePageContainers () {
  return (
    <div>
      <HeaderHeroMain/>
      <ButtonCategory/>
      <HomePageComponents2/>
    </div>
  )
}

export default HomePageContainers;