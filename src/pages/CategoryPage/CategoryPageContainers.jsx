import React from 'react'
import ButtonCategory from '../../common/buttonCategory'
import HeaderHeroMain from '../../common/header-hero-main'
import CategoryPageComponents from './components/CategoryPageComponents'

const CategoryPageContainers = () => {
  return (
    <div>
      <HeaderHeroMain/>
      <ButtonCategory/>
      <CategoryPageComponents/>
    </div>
  )
}

export default CategoryPageContainers
