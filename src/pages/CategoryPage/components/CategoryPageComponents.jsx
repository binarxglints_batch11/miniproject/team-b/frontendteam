import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchMovieAll } from '../../../state/showListAll/showListAllActions';
import { Link } from "react-router-dom";
import './CategoryPageComponents.css'
import { useParams } from 'react-router';

const CategoryPageComponents = () => {

  const {catID} = useParams()

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);   

  const states = useSelector((state) => state);

  const categories = useSelector (state =>
      state.showMovieAllReducer.moviesall.filter(o => Object.keys(o).some(k => String(o[k]).toLowerCase().includes(catID)))
  )

    const categoryPost = () => {
    if (states.showMovieAllReducer.loading) {
      var iniLoading = [...Array(10).keys()] 
      return iniLoading.map ((el) => {
        return (
          <a href = "#"  >
            <div className="film-item">
              <div className="film-cover hp-loading">
                <img src="" alt=""/>
              </div>
            </div>
          </a>
        )
      })
    }
      return categories.map ((el)=>{
        return (
          // <Link to = {`/view/${el._id}`}  >
            <div className="film-item">
              <div className="film-cover">
                <div className="overlay-category">
                  <Link to={`/view/${el._id}`}>{el.title}</Link>
                </div>
                <img src={`https://team-b.gabatch11.my.id${el.poster}`} alt={el.title}/>
              </div>
              <h3>{el.title}</h3>
            </div>
          // </Link>
        )
    })
  }

  return (
    <div className="category-container">
      <div className="category-wrapping">
        <div className="film-detail">
          {categoryPost()}
        </div>
        <div className="pagination-numb">
        </div>
      </div>
    </div>
  )
}

export default CategoryPageComponents
