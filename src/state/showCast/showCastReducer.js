import { FETCH_CAST_FAILURE, FETCH_CAST_REQUEST, FETCH_CAST_SUCCESS } from '../allType'

const showCastState = {
  loading: false,
  cast: [],
  error: null
}

const showCastReducer = (state= showCastState, action) => {
  switch (action.type){
    case FETCH_CAST_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_CAST_SUCCESS:
      return {
        ...state,
        loading: false,
        cast: action.payload
      }
    case FETCH_CAST_FAILURE:
      return {
        loading: false,
        error: action.error,
        cast: action.payload
      }
    default: return state
  }
}

export default showCastReducer
