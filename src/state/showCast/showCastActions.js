import Axios from "axios";
import { FETCH_CAST_FAILURE, FETCH_CAST_REQUEST, FETCH_CAST_SUCCESS } from "../allType";

export const fetchCast = () => async (dispatch, getState) => {
  dispatch ({type: FETCH_CAST_REQUEST})

  try {
    const response = await Axios.get (
      "https://team-b.gabatch11.my.id/cast"
    );
    dispatch({type: FETCH_CAST_SUCCESS,  payload: response.data["data"]})
    console.log(response.data)
  } catch (error) {
    dispatch ({type: FETCH_CAST_FAILURE, error})
  }
}