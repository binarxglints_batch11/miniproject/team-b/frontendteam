import { FETCH_MOVIE_FAILURE, FETCH_MOVIE_REQUEST, FETCH_MOVIE_SUCCESS } from "../allType"

const showMovieState = {
  loading: false,
  movies: [],
  error: null
}

const showMovieReducer = (state = showMovieState, action) => {
  switch (action.type) {
    case FETCH_MOVIE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_MOVIE_SUCCESS:
      return {
        ...state,
        loading: false,
        movies: action.payload
      }
    case FETCH_MOVIE_FAILURE:
      return {
        loading: false,
        error: action.error,
        movies: action.payload
      }
    default: return state
  }
}

export default showMovieReducer