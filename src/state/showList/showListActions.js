import Axios from "axios"
import { useSelector } from 'react-redux';
import { FETCH_MOVIE_FAILURE, FETCH_MOVIE_REQUEST, FETCH_MOVIE_SUCCESS } from "../allType"

export const fetchMovie = () => async (dispatch, getState) => {
  dispatch ({type: FETCH_MOVIE_REQUEST})
  try {
    const response = await Axios.get (
      "https://team-b.gabatch11.my.id/movie/", {
        params: {
          page: 1,
          limit: 18
        }
      }
    );
    
    dispatch ({type: FETCH_MOVIE_SUCCESS, payload: response.data["data"]})
    console.log(response.data)
  } catch (error) {
    dispatch ({type: FETCH_MOVIE_FAILURE, error})
  }
}