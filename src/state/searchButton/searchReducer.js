import { GET_SEARCH } from "../allType"

const initialSearch = {
  search:[]
}

function searchReducer (state = initialSearch, action) {
  switch (action.type) {
    case GET_SEARCH: 
    return { ...state,
      search: action.payload
    }
    default:
      return state
  }
}

export default searchReducer