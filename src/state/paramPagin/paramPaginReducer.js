const initialParamPagin = {
  page: 1,
  limit: 10
}

function paginReducer (state = initialParamPagin, action) {
  switch (action.type) {
    case "SET_FOR_SEARCH": 
    return { ...state,
      page: 1,
      limit: 999
    }
    default:
      return state
  }
}

export default paginReducer