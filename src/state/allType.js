export const FETCH_MOVIE_REQUEST = 'FETCH_MOVIE_REQUEST'
export const FETCH_MOVIE_SUCCESS = 'FETCH_MOVIE_SUCCESS'
export const FETCH_MOVIE_FAILURE = 'FETCH_MOVIE_FAILURE'

export const FETCH_CATEGORY_REQUEST = "FETCH_CATEGORY_REQUEST"
export const FETCH_CATEGORY_SUCCESS = "FETCH_CATEGORY_SUCCESS"
export const FETCH_CATEGORY_FAILURE = "FETCH_CATEGORY_FAILURE"

export const FETCH_CAST_REQUEST = "FETCH_CAST_REQUEST"
export const FETCH_CAST_SUCCESS = "FETCH_CAST_SUCCESS"
export const FETCH_CAST_FAILURE = "FETCH_CAST_FAILURE"

export const GET_SEARCH = "GET_SEARCH"