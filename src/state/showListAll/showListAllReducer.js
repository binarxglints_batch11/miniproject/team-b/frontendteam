const showMovieAllState = {
  loading: false,
  moviesall: [],
  error: null
}

const showMovieAllReducer = (state = showMovieAllState, action) => {
  switch (action.type) {
    case "FETCH_MOVIE_ALL_REQUEST":
      return {
        ...state,
        loading: true,
        error: null
      }
    case "FETCH_MOVIE_ALL_SUCCESS":
      return {
        ...state,
        loading: false,
        moviesall: action.payload
      }
    case "FETCH_MOVIE_ALL_FAILURE":
      return {
        loading: false,
        error: action.error,
        moviesall: action.payload
      }
    default: return state
  }
}

export default showMovieAllReducer