import Axios from "axios"

export const fetchMovieAll = () => async (dispatch, getState) => {
  dispatch ({type: "FETCH_MOVIE_ALL_REQUEST"})
  try {
    const response = await Axios.get (
      "https://team-b.gabatch11.my.id/movie/", {
        params: {
          limit: 999
        }
      }
    );
    
    dispatch ({type: "FETCH_MOVIE_ALL_SUCCESS", payload: response.data["data"]})
    console.log(response.data)
  } catch (error) {
    dispatch ({type: "FETCH_MOVIE_ALL_FAILURE", error})
  }
}