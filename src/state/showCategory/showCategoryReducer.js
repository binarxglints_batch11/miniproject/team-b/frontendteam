import { FETCH_CATEGORY_FAILURE, FETCH_CATEGORY_REQUEST, FETCH_CATEGORY_SUCCESS } from "../allType"

const showCategoryState = {
  loading: false,
  category: [],
  error: null
}

const showCategoryReducer = (state = showCategoryState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        category: action.payload
      }
    case FETCH_CATEGORY_FAILURE:
      return {
        loading: false,
        error: action.error,
        category: action.payload
      }
    default: return state
  }
}

export default showCategoryReducer