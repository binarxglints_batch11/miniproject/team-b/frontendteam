import Axios from "axios"
import { FETCH_CATEGORY_FAILURE, FETCH_CATEGORY_REQUEST, FETCH_CATEGORY_SUCCESS } from "../allType";

export const fetchCategory = () => async (dispatch, getState) => {
  dispatch ({type: FETCH_CATEGORY_REQUEST})

  try {
    const response = await Axios.get (
      "https://team-b.gabatch11.my.id/category"
    );
    
    dispatch ({type: FETCH_CATEGORY_SUCCESS, payload: response.data["data"]})
    console.log(response.data)
  } catch (error) {
    dispatch ({type: FETCH_CATEGORY_FAILURE, error})
  }
}
