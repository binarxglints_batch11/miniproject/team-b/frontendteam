import { combineReducers } from 'redux';
import showCategoryReducer from './showCategory/showCategoryReducer';
import showMovieReducer from './showList/showListReducer';
import showCastReducer from './showCast/showCastReducer';
import searchReducer from './searchButton/searchReducer';
import paginReducer from  './paramPagin/paramPaginReducer'
import showMovieAllReducer from './showListAll/showListAllReducer'

const rootReducer = combineReducers({
  showMovieReducer,
  showCategoryReducer,
  showCastReducer,
  searchReducer,
  paginReducer,
  showMovieAllReducer
})

export default rootReducer;