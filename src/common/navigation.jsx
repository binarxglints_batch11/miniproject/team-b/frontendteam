import React, { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux'
import './navigation.css';
import { Link, useHistory } from "react-router-dom";
import { GET_SEARCH } from '../state/allType';

function Navigation() {

  const [search, setSearch] = useState()
  const states = useSelector((state) => state)

  const dispatch = useDispatch()
  const history = useHistory()

  const handleChange = (e) => {
    setSearch(e.target.value);
  }

  const handleKeyDown = (e) => {
    if (e.which === 13) {
      
      dispatch({type: "SET_FOR_SEARCH"})
      dispatch({type: GET_SEARCH, payload: search })
      history.push(`/filter`)
      setSearch('')
    }
  }

  const signupButton = () => {

    if (localStorage.getItem('id') != null){
      return (
        <Link to = {'/profile'} >
          <div className="profile-image">
            <img src="" alt=""/>
          </div>
        </Link>)
    } else {
      return (
        <a href={"/signup"}>
          <button>Sign Up</button>
        </a>
      )
    }
  }


  return (
    <div className="navigation">
      <div className="navigation-wrapper">
        <div className="navigation-icon">
          <Link to={'/'}>
            <img src="https://i.ibb.co/8D2q2sr/Dark-Logo.png" alt="Dark Logo Wibu TV" />
          </Link>
        </div>
        <div className="navigation-search">
          <input
            placeholder="temukan anime kesukaan kamu ..."
            value={search}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
          />
        </div>
        <div className="navigation-account">
          <div className="navigation-account-login">
            {signupButton()}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Navigation;