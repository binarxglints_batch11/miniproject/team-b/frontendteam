import React, { useEffect } from 'react';
import './MovieTrailer.css';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { fetchMovieAll } from '../state/showListAll/showListAllActions';

export const MovieTrailer = ({ show, close }) => {
  
  const {filmID} = useParams()

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchMovieAll());
  },[]);

  const film = useSelector (state =>
    state.showMovieAllReducer.moviesall.find(filma => filma._id === filmID)
  )
  
  const trailer = () => {
    if (!film) {
      return (
        `https://www.youtube.com/embed/S8_YwFLCh4U`
      )
    } return (
      `${film.trailer}`
    )
  }

  return (
    <div
      className="trailer-modal-wrapper"
      style={{
        transform: show ? 'translateY(-45vh)' : 'translateY(-150vh)',
        opacity: show ? '1' : '0',
      }}
    >
      <div className="trailer-modal-header">
        <p>Play Trailer</p>
        <span onClick={close} className="close-modal-btn">
          x
        </span>
      </div>
      <div>
        <iframe
          width="800"
          height="335"
          src={trailer()}
          title="YouTube video player"
          frameborder=""
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
      </div>
    </div>
  );
};
