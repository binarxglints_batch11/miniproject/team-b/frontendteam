import React,{useEffect}from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchCategory } from '../state/showCategory/showCategoryActions';
import { Link } from 'react-router-dom';
import './buttonCategory.css'

const ButtonCategory = () => {

  const dispatch = useDispatch();
  const states = useSelector((state) => state);

  useEffect (() =>{
    dispatch(fetchCategory())
  },[])

  const buttonCategory = () => {
    if (states.showCategoryReducer.loading) {
      var categoryLoading = [...Array(10).keys()] 
      return categoryLoading.map ((el) => {
        return (
          <button className="buttonCateLoading">{el.category}</button>
        )
      })
      
    }
      return states.showCategoryReducer.category.map ((el) => {
        return (
          <Link to={`/category/${el._id}`}>
            <button>{el.category}</button>
          </Link>
        )
      })
  } 

  return (
    <div className="button-category-container">
      <div className="button-category-wrapping">
        <div className="button-category-category-bar">
          {buttonCategory()}
        </div>
      </div>
    </div>
  )
}

export default ButtonCategory
