import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';
import './header-hero.css';
import { MovieTrailer } from './MovieTrailer';
import ReadMoreReact from 'read-more-react';

const HeaderHero = () => {

  const [show, setShow] = useState(false);
  const closeModalHandler = () => setShow(false);

  const {filmID} = useParams()
  const film = useSelector (state =>
    state.showMovieAllReducer.moviesall.find(filma => filma._id === filmID)
  )

  const [dataReview, setDataReview] = useState([])

  useEffect(() => {
    fetch(`https://team-b.gabatch11.my.id/review/?movieId=${filmID}&page=1`)
      .then(res => res.json())
      .then(data => {
        setDataReview(data.data.docs)
      })
  })

  const title = () => {
    if (!film) {
      return (
      <h1>Film Tidak Ditemukan</h1>
      )
    } return (
      <h1>{film.title}</h1>
    )
  }

  const synopsis = () => {
    if (!film) {
      return (
        <div></div>
      )
    } return (
      // <div></div>
      <ReadMoreReact
        text={film.synopsis}
        min={150}
        ideal={300}
        max={450}
        readMoreText="Baca Lanjutan"/>
    )
  }

  const btnTrailer = () => {
    if (!film) {
      return (<div></div>)
    } return (
        <>
          {show ? <div onClick={closeModalHandler}></div> : null}
          <button onClick={() => setShow(true)} className="hero-btn-watch">
            Watch Trailer
          </button>
          <button className="hero-btn-add">
            Add to Watchlist
          </button>
        </>
    )
  }

  return (
  <>
    <div className="hero-banner">
      <div className="container-hero">
        <div className="hero-text">
          {title()}
          <div className="hero-rating">
            <p>&#9733; </p>
            <p>&#9733; </p>
            <p>&#9733; </p>
            <p>&#9733; </p>
            <p style={{ color: '#C4C4C4' }}>&#9733; </p>
            <p className ="hero-star" style={{ color: '#F4F4F4' }}>{`${dataReview.length} Reviews`}</p>
          </div>
          {synopsis()}
          {btnTrailer()}
        </div>
      </div>
    </div>
    <MovieTrailer show={show} close={closeModalHandler} />;
  </>
  )
};

export default HeaderHero;
