import React from 'react';
import './footer.css'

function Footer () {
  return (
    <div id="footer" className="footer">
      <div className="footer-wrapper">
        <div className="footer-upper-section">
          <div className="footer-left-column">
            <img src="https://i.ibb.co/yQjKcnG/Bright-Logo.png" alt="Logo Wibu TV" />
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem voluptates officiis quae cumque suscipit, earum excepturi recusandae dolore dolorum rerum laboriosam aut optio minus eaque architecto provident, possimus mollitia ipsam.</p>
          </div>
          <div className="footer-middle-column">
            <ul>
              <li><a href>Tentang Kami</a></li>
              <li><a href>Blog</a></li>
              <li><a href>Layanan</a></li>
              <li><a href>Karir</a></li>
              <li><a href>Pusat Media</a></li>
            </ul>
          </div>
          <div className="footer-right-column">
            <div className="footer-right-01">
              <h5>Unduh Aplikasi</h5>
              <div className="footer-download-group">
                <img src="https://i.ibb.co/3TvR0YC/google-play-store-icon-13.png" alt="google-play-store-icon-13" border="0"/>
                <img src="https://i.ibb.co/qRXfrQW/app-store-512.png" alt="app-store-512" border="0"/>
              </div>
            </div>
            <div className="footer-right-02">
              <h5>Sosial Media</h5>
              <div className="footer-social-group">
                <img src="https://i.ibb.co/41cqMRR/facebook-4-512.png" alt="Facebook Icon" border="0"/>
                <img src="https://i.ibb.co/QN36qdS/pinterest-4-512.png" alt="Pinterest Icon" border="0"/>
                <img src="https://i.ibb.co/bv0Q4k6/twitter-4-512.png" alt="Twitter Icon" border="0"/>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom-section">
          <p>Copyright 2000-2021 Wibu TV. All Rights Reserved</p>
        </div>
      </div>
    </div>
  );
}

export default Footer;