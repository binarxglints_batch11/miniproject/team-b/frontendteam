import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Footer from "./common/footer";
import Navigation from "./common/navigation";
import CategoryPageContainers from './pages/CategoryPage/CategoryPageContainers';
import CharacterPageContainers from './pages/CharacterPage/CharacterPageContainers';
import FilterPageContainers from './pages/FilterPage/FilterPageContainers';
import HomePageContainers from "./pages/HomePage/HomePageContainers";
import LoginSignUpContainers from "./pages/LoginPage/LoginSignUpContainers";
import ReviewPageContainers from './pages/ReviewPage/ReviewPageContainers';
import ViewPageContainers from './pages/ViewPage/ViewPageContainers';
import { history } from './state/history';
import './App.css';
import UserPageContainers from './pages/UserPage/UserPageContainers';

function App() {
  return (
    <Router history={history}>
      <div className="App">
        <Navigation/>
          <Switch>
            <Route exact path={['/','/home']} component={HomePageContainers}/>
            <Route exact path={'/signup'} component={LoginSignUpContainers}/>
            <Route exact path="/view/:filmID" component={ViewPageContainers}/>
            <Route exact path="/cast/:filmID" component={CharacterPageContainers}/>
            <Route exact path="/review/:filmID" component={ReviewPageContainers}/>
            <Route exact path={'/filter'} component={FilterPageContainers}/>
            <Route exact path="/category/:catID" component={CategoryPageContainers}/>
            <Route exact path="/profile" component={UserPageContainers}/>
          </Switch>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
